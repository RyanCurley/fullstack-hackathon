package com.grocery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grocery.demo.exception.ListAlreadyExistsException;
import com.grocery.demo.exception.ListNotFoundException;
import com.grocery.demo.model.GroceryList;
import com.grocery.demo.repository.GroceryListRepo;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class GroceryListService {
	
	private GroceryListRepo glRepo;
	
	public List<GroceryList> getAllGroceryLists() {
		return glRepo.findAllGroceryLists();
	}
	
	public GroceryList getGroceryListById(int listId) throws ListNotFoundException {
		if (glRepo.findById(listId)!=null) {
			return glRepo.findByListId(listId);			
		} else {
			throw new ListNotFoundException("Unable to find list!");
		}
	}
	
	public GroceryList getGroceryListByName(String listName) throws ListNotFoundException {
		if (glRepo.findByListName(listName)!=null) {
			return glRepo.findByListName(listName);			
		} else {
			throw new ListNotFoundException("Unable to find list!");
		}
	}
	
	public void insertGroceryList(GroceryList gList) throws ListAlreadyExistsException {
		if(glRepo.findByListName(gList.getListName())==null) {
			glRepo.save(gList);			
		} else {
			throw new ListAlreadyExistsException("Grocery List with that name already exists!");
		}
	}
	
	public void updateGroceryList(GroceryList gList) throws ListNotFoundException {
		if(glRepo.findById(gList.getListId())!=null) {
			glRepo.save(gList);			
		} else {
			throw new ListNotFoundException("Grocery list not found!");
		}
	}
	
	public void deleteGroceryList(GroceryList gList) throws ListNotFoundException {
		if(glRepo.findById(gList.getListId())!=null) {
			glRepo.delete(gList);			
		} else {
			throw new ListNotFoundException("Grocery list not found!");
		}
	}
	
}
