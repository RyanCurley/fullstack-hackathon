package com.grocery.controller;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grocery.exception.ItemAlreadyExistsException;
import com.grocery.model.GroceryItem;
import com.grocery.service.GroceryItemService;
import com.grocery.service.GroceryListService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@RestController
@RequestMapping(value = "/items")
@CrossOrigin(origins = "*")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@NoArgsConstructor
public class GroceryItemController {

	private GroceryItemService giServ;
	private GroceryListService glServ;
	
	@GetMapping("/all")
	public ResponseEntity<List<GroceryItem>> getAllGroceryLists(){
		List<GroceryItem> giList = giServ.getAllGroceryItems();
		if(giList.size()==0) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(giList, HttpStatus.OK);
		}
	}
	
	@GetMapping("/id/{itemId}")
	public ResponseEntity<GroceryItem> getGroceryListById(@PathVariable("itemId") int itemId){
		try {
			GroceryItem gItem = giServ.getGroceryItemById(itemId);			
			return new ResponseEntity<>(gItem, HttpStatus.OK);
		} catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin("*")
	@PostMapping("/create")
	public ResponseEntity<String> insertGroceryItem(@RequestBody LinkedHashMap glMap) {
		System.out.println("hitting item");
		try {
			GroceryItem gItem = new GroceryItem((String)glMap.get("item_name"), Integer.parseInt((String)glMap.get("item_cost")), glServ.getGroceryListById(Integer.parseInt((String)glMap.get("list_id"))));
			giServ.insertGroceryItem(gItem);
			System.out.println(gItem);
		} catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>("Grocery Item was unable to be inserted!", HttpStatus.NOT_ACCEPTABLE);
		}
		return new ResponseEntity<>("Grocery item successfully created!", HttpStatus.CREATED);
	}
	
	@CrossOrigin("*")
	@PutMapping()
	public ResponseEntity<GroceryItem> updateItem(@RequestBody LinkedHashMap giMap) {
		try {
			GroceryItem gItem = giServ.getGroceryItemById((Integer)giMap.get("item_id"));
			gItem.setItemName((String)giMap.get("item_name"));
			gItem.setItemCost((Integer)giMap.get("item_cost"));	
			return new ResponseEntity<>(gItem, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}
}
