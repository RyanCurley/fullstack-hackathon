package com.grocery.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "grocery_lists")
public class GroceryList {
	
	@Id
	@Column(name = "list_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int listId;
	
	@Column(name = "list_name")
	private String listName;
	
	@OneToMany(mappedBy="itemId", fetch=FetchType.LAZY)
	@JsonIgnore
	private List<GroceryItem> itemList = new ArrayList<>();
	
	
	public GroceryList(String listName, List<GroceryItem> itemList) {
		super();
		this.listName = listName;
		this.itemList = itemList;
	}
	
}
