package com.grocery.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.grocery.exception.ListAlreadyExistsException;
import com.grocery.model.GroceryList;
import com.grocery.service.GroceryListService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@RestController
@RequestMapping(value = "/lists")
@CrossOrigin(origins = "*")
@AllArgsConstructor(onConstructor = @__(@Autowired))
@NoArgsConstructor
public class GroceryListController {
	
	private GroceryListService gLServ;
	
	@GetMapping("/all")
	public ResponseEntity<List<GroceryList>> getAllGroceryLists(){
		List<GroceryList> glList = gLServ.getAllGroceryLists();
		if(glList.size()==0) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(glList, HttpStatus.OK);
		}
	}
	
	@GetMapping("/id/{listId}")
	public ResponseEntity<GroceryList> getGroceryListById(@PathVariable("listId") int listId){
		try {
			GroceryList gList = gLServ.getGroceryListById(listId);			
			return new ResponseEntity<>(gList, HttpStatus.OK);
		} catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@CrossOrigin("*")
	@PostMapping()
	public ResponseEntity<String> insertGroceryList(@RequestBody LinkedHashMap glMap) {
		System.out.println("hitting");
		GroceryList gList = new GroceryList((String)glMap.get("list_name"), new Timestamp(System.currentTimeMillis()));
		try {
			gLServ.insertGroceryList(gList);
			System.out.println(gList);
		} catch(ListAlreadyExistsException e) {
			e.printStackTrace();
			return new ResponseEntity<>("Grocery List with that name exists already!", HttpStatus.NOT_ACCEPTABLE);
		}
		return new ResponseEntity<>("Grocery list successfully created!", HttpStatus.CREATED);
	}
	
	@CrossOrigin("*")
	@PutMapping()
	public ResponseEntity<GroceryList> updateList(@RequestBody LinkedHashMap glMap) {
		try {
			GroceryList gList = gLServ.getGroceryListById((Integer)glMap.get("list_id"));
			gList.setListName((String)glMap.get("list_name"));
			gList.setLastChange(new Timestamp(System.currentTimeMillis()));			
			return new ResponseEntity<>(gList, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}
	
}
