package com.grocery.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grocery.model.GroceryList;

public interface GroceryListRepo extends JpaRepository<GroceryList, Integer>{

	public List<GroceryList> findAll();
	public GroceryList findByListId(int listId);
	public GroceryList findByListName(String listName);

}
