package com.grocery.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.grocery.model.GroceryItem;

public interface GroceryItemRepo extends JpaRepository <GroceryItem, Integer> {
	
	public List<GroceryItem> findAll();
	public List<GroceryItem> findByListId(int listId);
	public GroceryItem findByItemId(int itemId);
	public GroceryItem findByItemName(String itemName);

}
