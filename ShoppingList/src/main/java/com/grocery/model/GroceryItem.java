package com.grocery.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.grocery.model.GroceryList;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "grocery_items")
public class GroceryItem {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "item_id")
	private int itemId;
	
	@Column(name = "item_name")
	private String itemName;
	
	@Column(name = "item_cost")
	private int itemCost;
	
	@JoinColumn(name = "list_id")
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonIgnore
	private GroceryList listId;
	
	@Transient private int gListId;
	
	public void setUpFields() {
		this.gListId = listId.getListId();
	}
	
		
	public GroceryItem(String itemName, int itemCost, GroceryList gList) {
		super();
		this.itemName = itemName;
		this.itemCost = itemCost;
		this.listId = gList;
		setUpFields();
	}
}
