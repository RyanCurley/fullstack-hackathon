package com.grocery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grocery.exception.ItemAlreadyExistsException;
import com.grocery.exception.ItemNotFoundException;
import com.grocery.model.GroceryItem;
import com.grocery.repository.GroceryItemRepo;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class GroceryItemService {
	
	private GroceryItemRepo giRepo;
	
	public List<GroceryItem> getAllGroceryItems() {
		return giRepo.findAll();
	}
	
	public GroceryItem getGroceryItemById(int itemId) {
		return giRepo.findByItemId(itemId);
	}
	
	public GroceryItem getGroceryItemByName(String itemName) {
		return giRepo.findByItemName(itemName);
	}
	
	public List<GroceryItem> getGroceryItemByList(int listId) {
		return giRepo.findByListId(listId);
	}
	
	public void insertGroceryItem(GroceryItem gItem) throws ItemAlreadyExistsException {
		if(giRepo.findByItemId(gItem.getItemId())==null) {
			giRepo.save(gItem);
		} else {
			throw new ItemAlreadyExistsException("Item with that id already exists!");
		}
	}
	
	public void updateGroceryList(GroceryItem gItem) throws ItemNotFoundException {
		if(giRepo.findByItemId(gItem.getItemId())!=null) {
			giRepo.save(gItem);			
		} else {
			throw new ItemNotFoundException("Item could not be found!");
		}
	}
	
	public void deleteGroceryItem(GroceryItem gItem) throws ItemNotFoundException {
		if(giRepo.findByItemId(gItem.getItemId())!=null) {
			giRepo.delete(gItem);			
		} else {
			throw new ItemNotFoundException("Item could not be found!");
		}
	}
}